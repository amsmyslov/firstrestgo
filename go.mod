module awesomeProject

go 1.13

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lib/pq v1.2.0
	google.golang.org/appengine v1.6.4 // indirect
	gopkg.in/go-playground/validator.v9 v9.30.0
)
