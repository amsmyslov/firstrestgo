create table products_schema.products
(
    id    serial      not null
        constraint products_pk
            primary key,
    name  varchar(50) not null,
    price integer     not null
);

alter table products_schema.products
    owner to postgres;


