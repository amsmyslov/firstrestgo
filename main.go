package main

import (
	"log"
	"net/http"

	"gopkg.in/go-playground/validator.v9"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Product struct {
	Id    int    `db:"id" json:"id" validate:"required"`
	Name  string `db:"name" json:"name" validate:"required"`
	Price int    `db:"price" json:"price" validate:"gte=0,lte=130"`
}

func GetDbConnection() *sqlx.DB {
	db, err := sqlx.Connect("postgres", "user=postgres dbname=product_shop sslmode=disable port=32768")
	if err != nil {
		log.Fatalln(err)
	}

	return db
}

var validate *validator.Validate

func GetAllProduct(c *gin.Context) {
	var products []Product

	db := GetDbConnection()
	db.Select(&products, "SELECT * FROM products_schema.products")

	if len(products) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No todo found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": products})
}

func DeleteProduct(c *gin.Context) {
	productId := c.Param("id")

	db := GetDbConnection()
	sqlQuery := "DELETE FROM products_schema.products WHERE id = $1;"
	resultQuery, err := db.Exec(sqlQuery, productId)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "DB query error"})
		return
	}

	rowsAffected, err := resultQuery.RowsAffected()

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "DB affected error"})
		return
	}

	if rowsAffected == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No product found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "product deleted successfully!"})
}

func UpdateProduct(c *gin.Context) {

	var updateProduct Product

	//decoder := json.NewDecoder(c.Request.Body)
	//err := decoder.Decode(&updateProduct)

	err := c.ShouldBindJSON(&updateProduct)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "request json error", "data": err})
		return
	}

	//c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": updateProduct})

	//err := c.ShouldBindJSON(&updateProduct)

	errValidate := validate.Struct(&updateProduct)

	if errValidate != nil {
		outputErrors := ""
		for _, err := range errValidate.(validator.ValidationErrors) {
			outputErrors += err.StructNamespace() + "\n"
			outputErrors += err.Tag() + "\n"
			outputErrors += err.Param() + "\n"
			outputErrors += "\n"
		}
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "validate json error", "data": outputErrors})
		return
	}

	//productId := c.Param("id")
	//productName := c.PostForm("name")
	//productPrice := c.PostForm("price")

	//productId := updateProduct.Id
	//productName := updateProduct.Name
	//productPrice := updateProduct.Price

	//c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "productId": productId, "productName": productName, "productPrice": productPrice})

	db := GetDbConnection()

	sqlQuery := "UPDATE products_schema.products SET name=$1, price=$2 WHERE id = $3;"
	resultQuery, err := db.Exec(sqlQuery, updateProduct.Name, updateProduct.Price, updateProduct.Id)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "DB query error", "data": err})
		return
	}

	rowsAffected, err := resultQuery.RowsAffected()

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "DB affected error"})
		return
	}

	if rowsAffected == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No product found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Product updated successfully!"})

}

func CreateProduct(c *gin.Context) {

	var newProduct Product

	err := c.ShouldBindJSON(&newProduct)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "request json error", "data": err})
		return
	}

	db := GetDbConnection()

	sqlQuery := "INSERT INTO products_schema.products(name,price) VALUES($1,$2)"
	resultQuery, err := db.Exec(sqlQuery, newProduct.Name, newProduct.Price)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "DB query error", "data": err})
		return
	}

	rowsAffected, err := resultQuery.RowsAffected()

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "DB affected error"})
		return
	}

	if rowsAffected == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "Product wasn't created!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Product create successfully!"})

}

func main() {

	validate = validator.New()

	router := gin.Default()
	router.GET("/products/all", GetAllProduct)
	router.GET("/products/delete/:id", DeleteProduct)
	router.POST("/products/update", UpdateProduct)
	router.POST("/products/create", CreateProduct)

	router.Run() // listen and serve on 0.0.0.0:8080
}
